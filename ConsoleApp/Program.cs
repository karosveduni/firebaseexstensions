﻿using Fishman.Firebase.Extension;
using Fishman.Firebase.Firestore.Extension;

using ResultPattern;

Firebase.SetPathFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "key.json"));
Result r = Firebase.Init();

if (r)
{
	Console.WriteLine("init is complite");
}
else
{
	Console.WriteLine("Init is faild");
	Console.WriteLine(r.Message);
	return;
}

User user = new ("Oleg", 20);
Result<Google.Cloud.Firestore.WriteResult> result = await Firebase.FirestoreDb!.Collection("ColFirst").Document("DocFirst").SetValueAsync(user);

if (result)
{

	Console.WriteLine("set is valid");
	Console.WriteLine(result.Value!.UpdateTime);
}
else
{
	Console.WriteLine("Set is faild");
    Console.WriteLine(result.Message);
    return;
}

Result<User> resultUser = await Firebase.FirestoreDb.Collection("ColFirst").Document("DocFirst").GetValueAsync<User>();

if (resultUser)
{
	Console.WriteLine("get is valid");
	Console.WriteLine(resultUser.Value!.Name);
	Console.WriteLine(resultUser.Value.Age);
}
else
{
	Console.WriteLine("Get is faild");
	Console.WriteLine(result.Message);
	return;
}

class User(string name, int age)
{
	public string Name { get; set; } = name;
	public int Age { get; set; } = age;

	public User() : this("", 0)
	{

	}
}