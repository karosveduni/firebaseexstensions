﻿using System.Text.Json;

using FirebaseAdmin;

using Google.Apis.Auth.OAuth2;
using Google.Cloud.Firestore;

using ResultPattern;

namespace Fishman.Firebase.Extension
{
	/// <summary>
	/// Provides initialization and access to a Firestore database.
	/// </summary>
	public static class Firebase
	{
		/// <summary>
		/// Private field to store the FirestoreDb instance.
		/// </summary>
		private static FirestoreDb? _firestoreDb;

		/// <summary>
		/// Constant for the type of Firebase service account.
		/// </summary>
		public const string Type = "type";
		/// <summary>
		/// Constant for the Firestore project ID.
		/// </summary>
		public const string ProjectId = "project_id";

		/// <summary>
		/// Constant for the private key ID from Firebase service account.
		/// </summary>
		public const string PrivateKeyId = "private_key_id";
		/// <summary>
		/// Constant for the private key from Firebase service account.
		/// </summary>
		public const string PrivateKey = "private_key";
		/// <summary>
		/// Constant for the client email from Firebase service account.
		/// </summary>
		public const string ClientEmail = "client_email";
		/// <summary>
		/// Constant for the client ID from Firebase service account.
		/// </summary>
		public const string ClientId = "client_id";
		/// <summary>
		/// Constant for the authentication URI.
		/// </summary>
		public const string AuthUri = "auth_uri";
		/// <summary>
		/// Constant for the token URI.
		/// </summary>
		public const string TokenUri = "token_uri";
		/// <summary>
		/// Constant for the Auth Provider X509 Certificate URL.
		/// </summary>
		public const string AuthProviderX509CertUrl = "auth_provider_x509_cert_url";
		/// <summary>
		/// Constant for the Client X509 Certificate URL.
		/// </summary>
		public const string ClientX509CertUrl = "client_x509_cert_url";
		/// <summary>
		/// Constant for the universe domain.
		/// </summary>
		public const string UniverseDomain = "universe_domain";

		/// <summary>
		/// Constant for the environment variable name for Google application credentials.
		/// </summary>
		public const string GoogleApplicationCredentials = "GOOGLE_APPLICATION_CREDENTIALS";

		/// <summary>
		/// Gets the FirestoreDb instance.
		/// </summary>
		public static FirestoreDb? FirestoreDb => _firestoreDb;

		/// <summary>
		/// Gets or sets the path to the Firebase configuration JSON file.
		/// </summary>
		public static string? Path { get; private set; }

		/// <summary>
		/// Sets the path to the Firebase configuration JSON file and updates the environment variable.
		/// </summary>
		/// <param name="path">The file path to the Firebase configuration JSON.</param>
		/// <returns>A result indicating the success or failure of the operation.</returns>
		public static Result SetPathFile(string path)
		{
			try
			{
				Environment.SetEnvironmentVariable(GoogleApplicationCredentials, path);
				Path = path;

				return Result.Success();
			}
			catch (Exception ex)
			{
				return Result.Failure(ex.Message);
			}
		}

		/// <summary>
		/// Initializes the Firebase Firestore using the specified JSON file path.
		/// </summary>
		/// <param name="path">The path to the JSON file containing Firebase configuration.</param>
		/// <returns>A result indicating success or failure.</returns>
		public static Result Init()
		{
			Result<string> projectIdResult = GetProjectId();

			if (projectIdResult)
			{
				return Init(projectIdResult.Value!);
			}
			else
			{
				return Result.Failure(projectIdResult.Message!);
			}
		}

		/// <summary>
		/// Initializes the Firebase Firestore using the specified JSON file path and project ID.
		/// </summary>
		/// <param name="path">The path to the JSON file containing Firebase configuration.</param>
		/// <param name="projectID">The Firestore project ID.</param>
		/// <returns>A result indicating success or failure.</returns>
		public static Result Init(string projectID)
		{
			try
			{
				FirebaseApp.Create(new AppOptions()
				{
					Credential = GoogleCredential.FromFile(Path),
				});

				_firestoreDb = FirestoreDb.Create(projectID);

				return Result.Success();
			}
			catch (Exception ex)
			{
				return Result.Failure(ex.Message);
			}
		}

		/// <summary>
		/// Retrieves the project ID from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the project ID if successful, or an error message if not.</returns>
		public static Result<string> GetProjectId() => GetProperty(ProjectId);

		/// <summary>
		/// Retrieves the type of Firebase service account from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the type if successful, or an error message if not.</returns>
		public static Result<string> GetType() => GetProperty(Type);

		/// <summary>
		/// Retrieves the private key ID from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the private key ID if successful, or an error message if not.</returns>
		public static Result<string> GetPrivateKeyId() => GetProperty(PrivateKeyId);

		/// <summary>
		/// Retrieves the private key from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the private key if successful, or an error message if not.</returns>
		public static Result<string> GetPrivateKey() => GetProperty(PrivateKey);

		/// <summary>
		/// Retrieves the client email from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the client email if successful, or an error message if not.</returns>
		public static Result<string> GetClientEmail() => GetProperty(ClientEmail);

		/// <summary>
		/// Retrieves the client ID from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the client ID if successful, or an error message if not.</returns>
		public static Result<string> GetClientId() => GetProperty(ClientId);

		/// <summary>
		/// Retrieves the authentication URI from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the authentication URI if successful, or an error message if not.</returns>
		public static Result<string> GetAuthUri() => GetProperty(AuthUri);

		/// <summary>
		/// Retrieves the token URI from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the token URI if successful, or an error message if not.</returns>
		public static Result<string> GetTokenUri() => GetProperty(TokenUri);

		/// <summary>
		/// Retrieves the Auth Provider X509 Certificate URL from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the Auth Provider X509 Certificate URL if successful, or an error message if not.</returns>
		public static Result<string> GetAuthProviderX509CertUrl() => GetProperty(AuthProviderX509CertUrl);

		/// <summary>
		/// Retrieves the Client X509 Certificate URL from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the Client X509 Certificate URL if successful, or an error message if not.</returns>
		public static Result<string> GetClientX509CertUrl() => GetProperty(ClientX509CertUrl);

		/// <summary>
		/// Retrieves the universe domain from the Firebase configuration JSON file.
		/// </summary>
		/// <returns>A result containing the universe domain if successful, or an error message if not.</returns>
		public static Result<string> GetUniverseDomain() => GetProperty(UniverseDomain);

		/// <summary>
		/// Retrieves the value of a specified property from the Firebase configuration JSON file.
		/// </summary>
		/// <param name="property">The name of the property to retrieve.</param>
		/// <returns>A result containing the property value if successful, or an error message if not.</returns>
		public static Result<string> GetProperty(string property)
		{
			try
			{
				string json = File.ReadAllText(Path);
				JsonDocument doc = JsonDocument.Parse(json);
				JsonElement root = doc.RootElement;
				string projectId = root.GetProperty(ProjectId).GetString();

				return Result.Success(projectId);
			}
			catch (Exception ex)
			{
				return Result.Failure(ex.Message, string.Empty);
			}
		}
	}
}