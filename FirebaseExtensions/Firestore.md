# Firebase/Firestore/Exstension

**A library has been created to simplify data retrieval and uploading in Firestore.**

# FirebaseExtension Code Description

## Overview

The `FirebaseExtension` class provides extension methods for `DocumentReference` to perform Firestore operations. These methods facilitate setting and retrieving document data asynchronously, with options for customization and error handling.

## Methods

### SetValueAsync Methods

1. **SetValueAsync\<T\>(this DocumentReference documentReference, T value)**
    - Sets a document's data in Firestore.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `value`: The value to set in the document.
    - **Returns:** A task representing the result of the write operation.

2. **SetValueAsync\<T\>(this DocumentReference documentReference, T value, SetOptions? setOptions)**
    - Sets a document's data in Firestore with specified set options.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `value`: The value to set in the document.
        - `setOptions`: The options for setting the value.
    - **Returns:** A task representing the result of the write operation.

3. **SetValueAsync\<T\>(this DocumentReference documentReference, T value, CancellationToken cancellationToken)**
    - Sets a document's data in Firestore with a cancellation token.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `value`: The value to set in the document.
        - `cancellationToken`: A token to monitor for cancellation requests.
    - **Returns:** A task representing the result of the write operation.

4. **SetValueAsync\<T\>(this DocumentReference documentReference, T value, SetOptions? setOptions, CancellationToken cancellationToken)**
    - Sets a document's data in Firestore with specified set options and a cancellation token.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `value`: The value to set in the document.
        - `setOptions`: The options for setting the value.
        - `cancellationToken`: A token to monitor for cancellation requests.
    - **Returns:** A task representing the result of the write operation.

### GetValueAsync Methods

1. **GetValueAsync\<T\>(this DocumentReference documentReference) where T : new()**
    - Retrieves a document's data from Firestore.
    - **Parameters:**
        - `documentReference`: The document reference.
    - **Returns:** A task representing the result of the read operation.

2. **GetValueAsync\<T\>(this DocumentReference documentReference, CancellationToken cancellationToken) where T : new()**
    - Retrieves a document's data from Firestore with a cancellation token.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `cancellationToken`: A token to monitor for cancellation requests.
    - **Returns:** A task representing the result of the read operation.

## Error Handling

- The methods return a `Result` object that indicates the success or failure of the operation.
- If the `value` or `documentReference` parameters are null, the `SetValueAsync` methods return a failed `Result` with an appropriate error message.
- Exceptions encountered during the operations are caught and result in a failed `Result` with the exception message.

# Firebase Initialization Example

## Overview

This document provides an example of how to initialize the Firebase Firestore database using the `Firebase.Init` method with a provided JSON credentials file. The example demonstrates the process of combining the base directory path with the JSON key file to create the necessary initialization.

## Example Usage

### Initialization Code

```csharp
// Combine the base directory with the key.json file to get the full path
string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "key.json");

// Initialize Firebase using the path to the credentials file
Result r = Firebase.Init(path);

// Check the result of the initialization
if (r.IsSuccess)
{
    Console.WriteLine("Firebase initialized successfully.");
}
else
{
    Console.WriteLine($"Error initializing Firebase: {r.Error}");
}
```

## Usage Example

```csharp
var documentReference = firestoreDb.Collection("collectionName").Document("documentName");

// Setting a value
var result = await documentReference.SetValueAsync(new { Name = "John Doe", Age = 30 });
if (result.IsSuccess)
{
    Console.WriteLine("Document updated successfully.");
}
else
{
    Console.WriteLine($"Error: {result.Error}");
}

// Getting a value
var getResult = await documentReference.GetValueAsync<MyClass>();
if (getResult.IsSuccess)
{
    var myObject = getResult.Value;
    Console.WriteLine($"Document retrieved: {myObject.Name}, {myObject.Age}");
}
else
{
    Console.WriteLine($"Error: {getResult.Error}");
}
```

# Library version 1.2.0

In the new library version 1.2.0, the following functions and constants have been added:

- Constants for identifying various Firebase configuration parameters, including the account type, project ID, private key ID and key, client email and ID, authentication and token URIs, X509 certificate URLs for the authentication provider and client, as well as the universe domain.

- Methods for retrieving the values of these parameters from the Firebase configuration JSON file, including `GetType()`, `GetPrivateKeyId()`, `GetPrivateKey()`, `GetClientEmail()`, `GetClientId()`, `GetAuthUri()`, `GetTokenUri()`, `GetAuthProviderX509CertUrl()`, `GetClientX509CertUrl()`, and `GetUniverseDomain()`.

These changes facilitate working with Firebase configuration, allowing developers to easily extract the necessary parameters for initializing and working with Firebase Firestore.
