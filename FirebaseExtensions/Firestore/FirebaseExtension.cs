﻿using System.Reflection;

using Google.Cloud.Firestore;

using ResultPattern;

namespace Fishman.Firebase.Firestore.Extension
{
	/// <summary>
	/// Provides extension methods for Firestore DocumentReference operations.
	/// </summary>
	public static class FirebaseExtension
	{
		/// <summary>
		/// Asynchronously sets a document's data in Firestore.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="documentReference">The document reference.</param>
		/// <param name="value">The value to set in the document.</param>
		/// <returns>A task representing the result of the write operation.</returns>
		public static Task<Result<WriteResult>> SetValueAsync<T>(this DocumentReference documentReference, T value) => documentReference.SetValueAsync(value, default, CancellationToken.None);

		/// <summary>
		/// Asynchronously sets a document's data in Firestore with specified set options.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="documentReference">The document reference.</param>
		/// <param name="value">The value to set in the document.</param>
		/// <param name="setOptions">The options for setting the value.</param>
		/// <returns>A task representing the result of the write operation.</returns>
		public static Task<Result<WriteResult>> SetValueAsync<T>(this DocumentReference documentReference, T value, SetOptions? setOptions) => documentReference.SetValueAsync(value, setOptions, CancellationToken.None);

		/// <summary>
		/// Asynchronously sets a document's data in Firestore with a cancellation token.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="documentReference">The document reference.</param>
		/// <param name="value">The value to set in the document.</param>
		/// <param name="cancellationToken">A token to monitor for cancellation requests.</param>
		/// <returns>A task representing the result of the write operation.</returns>
		public static Task<Result<WriteResult>> SetValueAsync<T>(this DocumentReference documentReference, T value, CancellationToken cancellationToken) => documentReference.SetValueAsync(value, default, cancellationToken);

		/// <summary>
		/// Asynchronously sets a document's data in Firestore with specified set options and a cancellation token.
		/// </summary>
		/// <typeparam name="T">The type of the value to set.</typeparam>
		/// <param name="documentReference">The document reference.</param>
		/// <param name="value">The value to set in the document.</param>
		/// <param name="setOptions">The options for setting the value.</param>
		/// <param name="cancellationToken">A token to monitor for cancellation requests.</param>
		/// <returns>A task representing the result of the write operation.</returns>
		public static async Task<Result<WriteResult>> SetValueAsync<T>(this DocumentReference documentReference, T value, SetOptions? setOptions, CancellationToken cancellationToken)
		{
			if (value is null)
				return Result<WriteResult>.Failure("Value cannot be null");

			if (documentReference is null)
				return Result<WriteResult>.Failure("Document reference cannot be null");

			Dictionary<string, object> valuePairs = [];

			try
			{
				foreach (PropertyInfo property in value.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
				{
					if (property.CanRead)
						valuePairs.Add(property.Name, property.GetValue(value)!);
				}
			}
			catch (Exception ex)
			{
				return Result<WriteResult>.Failure(ex.Message);
			}

			WriteResult writeResult = await documentReference.SetAsync(valuePairs, setOptions, cancellationToken);

			return Result.Success(writeResult);
		}

		/// <summary>
		/// Asynchronously retrieves a document's data from Firestore.
		/// </summary>
		/// <typeparam name="T">The type of the value to retrieve.</typeparam>
		/// <param name="documentReference">The document reference.</param>
		/// <returns>A task representing the result of the read operation.</returns>
		public static Task<Result<T>> GetValueAsync<T>(this DocumentReference documentReference) where T : new() => documentReference.GetValueAsync<T>(CancellationToken.None);

		/// <summary>
		/// Asynchronously retrieves a document's data from Firestore with a cancellation token.
		/// </summary>
		/// <typeparam name="T">The type of the value to retrieve.</typeparam>
		/// <param name="documentReference">The document reference.</param>
		/// <param name="cancellationToken">A token to monitor for cancellation requests.</param>
		/// <returns>A task representing the result of the read operation.</returns>
		public static async Task<Result<T>> GetValueAsync<T>(this DocumentReference documentReference, CancellationToken cancellationToken) where T : new()
		{
			T value = new();
			DocumentSnapshot snapshot = await documentReference.GetSnapshotAsync(cancellationToken);

			try
			{
				Dictionary<string, object> obj = snapshot.ToDictionary();

				foreach (var property in value.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
				{
					if (property.CanWrite)
						property.SetValue(value, Convert.ChangeType(obj[property.Name], property.PropertyType));
				}

			}
			catch (Exception ex)
			{
				return Result<T>.Failure(ex.Message);
			}

			return Result.Success(value);
		}
	}
}
