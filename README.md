# Firebase/Firestore/Exstension

**A library has been created to simplify data retrieval and uploading in Firestore.**

# FirebaseExtension Code Description

## Overview

The `FirebaseExtension` class provides extension methods for `DocumentReference` to perform Firestore operations. These methods facilitate setting and retrieving document data asynchronously, with options for customization and error handling.

## Methods

### SetValueAsync Methods

1. **SetValueAsync\<T\>(this DocumentReference documentReference, T value)**
    - Sets a document's data in Firestore.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `value`: The value to set in the document.
    - **Returns:** A task representing the result of the write operation.

2. **SetValueAsync\<T\>(this DocumentReference documentReference, T value, SetOptions? setOptions)**
    - Sets a document's data in Firestore with specified set options.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `value`: The value to set in the document.
        - `setOptions`: The options for setting the value.
    - **Returns:** A task representing the result of the write operation.

3. **SetValueAsync\<T\>(this DocumentReference documentReference, T value, CancellationToken cancellationToken)**
    - Sets a document's data in Firestore with a cancellation token.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `value`: The value to set in the document.
        - `cancellationToken`: A token to monitor for cancellation requests.
    - **Returns:** A task representing the result of the write operation.

4. **SetValueAsync\<T\>(this DocumentReference documentReference, T value, SetOptions? setOptions, CancellationToken cancellationToken)**
    - Sets a document's data in Firestore with specified set options and a cancellation token.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `value`: The value to set in the document.
        - `setOptions`: The options for setting the value.
        - `cancellationToken`: A token to monitor for cancellation requests.
    - **Returns:** A task representing the result of the write operation.

### GetValueAsync Methods

1. **GetValueAsync\<T\>(this DocumentReference documentReference) where T : new()**
    - Retrieves a document's data from Firestore.
    - **Parameters:**
        - `documentReference`: The document reference.
    - **Returns:** A task representing the result of the read operation.

2. **GetValueAsync\<T\>(this DocumentReference documentReference, CancellationToken cancellationToken) where T : new()**
    - Retrieves a document's data from Firestore with a cancellation token.
    - **Parameters:**
        - `documentReference`: The document reference.
        - `cancellationToken`: A token to monitor for cancellation requests.
    - **Returns:** A task representing the result of the read operation.

## Error Handling

- The methods return a `Result` object that indicates the success or failure of the operation.
- If the `value` or `documentReference` parameters are null, the `SetValueAsync` methods return a failed `Result` with an appropriate error message.
- Exceptions encountered during the operations are caught and result in a failed `Result` with the exception message.

## Example Usage

### Initialization Code

```csharp
// Combine the base directory with the key.json file to get the full path
string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "key.json");

// Initialize Firebase using the path to the credentials file
Result r = Firebase.Init(path);

// Check the result of the initialization
if (r.IsSuccess)
{
    Console.WriteLine("Firebase initialized successfully.");
}
else
{
    Console.WriteLine($"Error initializing Firebase: {r.Error}");
}
```

## Usage Example

```csharp
var documentReference = firestoreDb.Collection("collectionName").Document("documentName");

// Setting a value
var result = await documentReference.SetValueAsync(new { Name = "John Doe", Age = 30 });
if (result.IsSuccess)
{
    Console.WriteLine("Document updated successfully.");
}
else
{
    Console.WriteLine($"Error: {result.Error}");
}

// Getting a value
var getResult = await documentReference.GetValueAsync<MyClass>();
if (getResult.IsSuccess)
{
    var myObject = getResult.Value;
    Console.WriteLine($"Document retrieved: {myObject.Name}, {myObject.Age}");
}
else
{
    Console.WriteLine($"Error: {getResult.Error}");
}
```